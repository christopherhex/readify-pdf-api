var { Readability } = require('@mozilla/readability');
const { JSDOM } = require('jsdom');
const puppeteer = require('puppeteer');
const chrome = require('chrome-aws-lambda');

const styleHTML = (title, content) => {

  const style = `
    <style>
      .h1 {
        margin-top: 40px;
      }
      
      .page {
        font-family: Helvetica, Arial, sans-serif;
        line-height: 1.5em;
        margin: 20px;
        font-size: 12pt;
      }
      
      .page img {
        display: block;
        margin-left: auto;
        margin-right: auto;
        max-width: 50%;
        text-align: center;
      }
      
      .page figcaption {
        font-style: italic;
        text-align: center;
      }
    </style>
  `;

  const htmlData = `
    <html>
      <head>
      </head>
      <body>
        <h1>${title}</h1>
        ${content}
        ${style}
      </body>
    </html>

  `

  return htmlData;
}


exports.convertWebsiteToReadablePDF = async (url, options = {}) => {

  // Step 1 - Open browser and go to URL
  const browser = await puppeteer.launch({
    args: chrome.args,
    executablePath: await chrome.executablePath,
    headless: chrome.headless
});
  const page = await browser.newPage();
  await page.goto(url, {waitUntil: 'networkidle2'});
  const bodyHTML = await page.evaluate(() => new XMLSerializer().serializeToString(document));

  // Step 2 - Parse Article
  const doc = new JSDOM(bodyHTML, { url });
  let reader = new Readability(doc.window.document);
  let article = reader.parse();

  const { title, content } = article;
  // Step 3 - Add Styling 
  const styledHTML = styleHTML(title, content);

  // Step 4 - Update Page and Close
  await page.setContent(styledHTML, {waitUntil: 'networkidle2'});
  const pdfData = await page.pdf({ format: 'A4'});

  await browser.close();

  return { pdf: pdfData, title };
}