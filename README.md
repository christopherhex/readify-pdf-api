# Readify PDF

This is a simple serverless function that creates a reader-PDF from a website URL.  
It uses the Mozilla [readability](https://github.com/mozilla/readability) library. 
The serverless function is hosted on [Vercel](https://vercel.com)


## Use Cases
I use it in combination with Apple Shortcuts on my phone to easily create reader-PDF's and send them to my note taking app Notability in one click.

## Example

https://readify-pdf-api.vercel.app/api/convert?url=http%3A%2F%2Fpaulgraham.com%2Fearly.html
