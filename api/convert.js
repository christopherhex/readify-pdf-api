const { convertWebsiteToReadablePDF } = require('../lib/convert');
const { Readable } = require('stream');


module.exports = (req, res) => {
  const { url } = req.query;

  (async () => {
    const { pdf, title } = await convertWebsiteToReadablePDF(url);

    const streamData = Readable.from(pdf);
    console.log(pdf.length);
    res.writeHead(200, {
      'Content-Length': Buffer.byteLength(pdf),
      'Content-Type' : 'application/pdf',
      'Content-disposition' : `attachment;filename="${title}.pdf"`
    })
    streamData.pipe(res);
  })().catch(err => {
    console.log(err);
    res.status(500).send(err);
  });

  
}